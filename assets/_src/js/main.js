$(function() {

    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if (isSafari) {
        $('html').addClass('_safari');
    };

    var ua = window.navigator.userAgent;
    var iOS = !!ua.match(/iPad/i) || !!ua.match(/iPhone/i);
    var webkit = !!ua.match(/WebKit/i);
    var iOSSafari = iOS && webkit && !ua.match(/CriOS/i);

    function createFullpage() {
        if (iOSSafari) {
            $('#fullpage').fullpage({
                fixedElements: '.js-modal',
                anchors: ['intro', 'news'],
                onLeave: function(origin, destination, direction) {
                    if (origin === 1) {
                        $('#header').removeClass('_dark');
                    } else if (origin === 2 && direction === 'up') {
                        $('#header').addClass('_dark');
                    }
                },
                //menu: '#menu',
                fitToSection: true,
                keyboardScrolling: true,
                animateAnchor: true,
                verticalCentered: false,
                scrollOverflow: true,
                scrollOverflowOptions: {
                    click:true,
                    tap: true
                },
                resize: true,
                normalScrollElements: '.popup'
            });

            $('.form__label').on('click', function () {
                $(this).prev().prop('checked', true);
            });
        } else {
            $('#fullpage').fullpage({
                fixedElements: '#header, .js-modal',
                anchors: ['intro', 'news'],
                onLeave: function(origin, destination, direction) {
                    if (origin === 1) {
                        $('#header').removeClass('_dark');
                    } else if (origin === 2 && direction === 'up') {
                        $('#header').addClass('_dark');
                    }
                },
                //menu: '#menu',
                fitToSection: true,
                keyboardScrolling: true,
                animateAnchor: true,
                verticalCentered: false,
                scrollOverflow: true,
                scrollOverflowOptions: {
                    click:false,
                    tap: true
                },
                resize: true,
                normalScrollElements: '.popup'
            });
        }
    }

    createFullpage();

    $('.js-modal-trigger').on('click', function() {
        $('.js-modal').addClass('show');
        $('body').addClass('popup-open');
        $('.section').addClass('blur');
    });

    $('.js-close').on('click', function() {
        $(this).closest('.js-modal').removeClass('show');
        $('body').removeClass('popup-open');
        $('.section').removeClass('blur');
    });

    var newsSlider = new Swiper('.swiper-container', {
        navigation: {
            prevEl: '.popup-slider-wrapper .swiper-button-prev',
            nextEl: '.popup-slider-wrapper .swiper-button-next',
        },
        on: {
            resize: function() {
                newsSlider.update();
            }
        }
    });

    $('.js-sorting-label').on('click', function() {
        $(this).closest('.sorting').toggleClass('opened');
    });

    var shareTrigger = $('.js-share-trigger');

    shareTrigger.hover(
        function() {
            $('.js-share').addClass('show');
        },
        function() {
            setTimeout(function() {
                $('.js-share').removeClass('show');
            }, 400);
        }
    );

    $('.news').find('.js-share-trigger').on('click', function (e) {
        e.preventDefault();

        if ($(window).width() < 800){
            $('.js-share').toggleClass('show');
        }
    });

    $(document).on('click', function (e) {
        if (!$(e.target).is('.js-share') && !$(e.target).is('svg') && !$(e.target).is('.js-share-trigger') && $(window).width() < 800){
            $('.js-share').removeClass('show');
        }
    });
});
